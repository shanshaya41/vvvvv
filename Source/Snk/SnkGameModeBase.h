// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnkGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNK_API ASnkGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
